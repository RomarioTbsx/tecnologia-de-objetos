/*
 * Empleado.cpp
 *
 *  Created on: 22 de abr. de 2016
 *      Author: romario
 */

#include "Empleado.h"

Empleado::Empleado() {
	// TODO Auto-generated constructor stub

}

Empleado::~Empleado() {
	// TODO Auto-generated destructor stub
}

Empleado::Empleado(string nombre,string seguro,double horasTrabajas,string telefono){
	this->nombre=nombre;
	this->seguro=seguro;
	this->horasTrabajas=horasTrabajas;
	this->telefono=telefono;
}

string Empleado::getNombre(){
	return nombre;
}

string Empleado::getSeguro(){
	return seguro;
}

double Empleado::getSalario(){
	double salario=45;
	salario=salario*horasTrabajas;
	return salario;
}

string Empleado::getTelefono(){
	return telefono;
}

void Empleado::show(){
	cout<<"********EMPLEADO*******"<<endl;
	cout<<"Nombre: "<<getNombre()<<endl;
	cout<<"Seguro: "<<getSeguro()<<endl;
	cout<<"Salario: $"<<getSalario()<<endl;
	cout<<"Telefono: "<<getTelefono()<<endl;
}
