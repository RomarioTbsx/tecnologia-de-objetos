/*
 * Empleado.h
 *
 *  Created on: 22 de abr. de 2016
 *      Author: romario
 */

#ifndef EMPLEADO_H_
#define EMPLEADO_H_

#include <iostream>
using namespace std;

class Empleado {
	string nombre;
	double horasTrabajas;
	string seguro;
	string telefono;
public:
	Empleado();
	~Empleado();
	Empleado(string,string,double,string);
	string getNombre();
	string getSeguro();
	double getSalario();
	string getTelefono();
	void show();
};

#endif /* EMPLEADO_H_ */
