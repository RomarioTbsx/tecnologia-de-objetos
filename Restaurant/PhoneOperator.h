/*
 * PhoneOperator.h
 *
 *  Created on: 22 de abr. de 2016
 *      Author: romario
 */

#ifndef PHONEOPERATOR_H_
#define PHONEOPERATOR_H_

#include "Empleado.h"

class PhoneOperator:public Empleado {
public:
	PhoneOperator();
	~PhoneOperator();
	PhoneOperator(string,string,double,string);
	void show();
};

#endif /* PHONEOPERATOR_H_ */
