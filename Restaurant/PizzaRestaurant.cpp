/*
 * PizzaRestaurant.cpp
 *
 *  Created on: 21 de abr. de 2016
 *      Author: romario
 */

#include "PizzaRestaurant.h"

PizzaRestaurant::PizzaRestaurant() {
	// TODO Auto-generated constructor stub

}

PizzaRestaurant::~PizzaRestaurant() {
	// TODO Auto-generated destructor stub
}

PizzaRestaurant::PizzaRestaurant(string nombre,string direccion,string telefono){
	this->nombre=nombre;
	this->direccion=direccion;
	this->telefono=telefono;
	//this->menu=menu;
}

string PizzaRestaurant::getNombre(){
	return nombre;
}

string PizzaRestaurant::getDireccion(){
	return direccion;
}

string PizzaRestaurant::getTelefono(){
	return telefono;
}

void PizzaRestaurant::setMenu(Menu _menu){
	menu=_menu;
}

void PizzaRestaurant::setEmpleado(Empleado _empleado){
	empleado=_empleado;
}

void PizzaRestaurant::setCook(Cook _cook){
	cook=_cook;
}

void PizzaRestaurant::setDeliveryMan(DeliveryMan _dman){
	dman=_dman;
}

void PizzaRestaurant::setPhoneOperator(PhoneOperator _phone){
	phone=_phone;
}

void PizzaRestaurant::show(){
	cout<<"******RESTAURANTE******"<<endl;
	cout<<"Pizzeria: "<<getNombre()<<endl;
	cout<<"Direccion: "<<getDireccion()<<endl;
	cout<<"Telefono: "<<getTelefono()<<endl;

}
void PizzaRestaurant::showMenu(){
	cout<<"*********Pedido********"<<endl;
	cout<<"Pizza: "<<menu.getPizza()<<endl;
	cout<<"Cliente: "<<menu.getCliente()<<endl;
	cout<<"Mesa: "<<menu.getNumMesa()<<endl;
}

void PizzaRestaurant::showEmpleado(){
	empleado.show();
}

void PizzaRestaurant::showCook(){
	cook.show();
}

void PizzaRestaurant::showPhoneOperator(){
	phone.show();
}

void PizzaRestaurant::showDeliveryMan(){
	dman.show();
}
