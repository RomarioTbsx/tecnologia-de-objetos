/*
 * PizzaRestaurant.h
 *
 *  Created on: 21 de abr. de 2016
 *      Author: romario
 */

#ifndef PIZZARESTAURANT_H_
#define PIZZARESTAURANT_H_

#include <iostream>
#include "Menu.h"
#include "Empleado.h"
#include "DeliveryMan.h"
#include "Cook.h"
#include "PhoneOperator.h"
using namespace std;

class PizzaRestaurant {
	string nombre;
	string direccion;
	string telefono;
	Menu menu;
	Empleado empleado;
	DeliveryMan dman;
	Cook cook;
	PhoneOperator phone;

public:
	PizzaRestaurant();
	~PizzaRestaurant();
	PizzaRestaurant(string,string,string);
	string getNombre();
	string getDireccion();
	string getTelefono();
	void setMenu(Menu);
	void setEmpleado(Empleado);
	void setDeliveryMan(DeliveryMan);
	void setCook(Cook);
	void setPhoneOperator(PhoneOperator);
	void show();
	void showMenu();
	void showEmpleado();
	void showDeliveryMan();
	void showPhoneOperator();
	void showCook();

};

#endif /* PIZZARESTAURANT_H_ */
