/*
 * Cook.h
 *
 *  Created on: 22 de abr. de 2016
 *      Author: romario
 */

#ifndef COOK_H_
#define COOK_H_

#include "Empleado.h"
class Cook:public Empleado {
public:
	Cook();
	~Cook();
	Cook(string,string,double,string);
	void show();
};

#endif /* COOK_H_ */
