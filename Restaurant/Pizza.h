/*
 * Pizza.h
 *
 *  Created on: 22 de abr. de 2016
 *      Author: romario
 */

#ifndef PIZZA_H_
#define PIZZA_H_

#include <iostream>
using namespace std;

class Pizza {
	string nomPizza;
public:
	Pizza();
	~Pizza();
	Pizza(string);
	string getPizza();

};

#endif /* PIZZA_H_ */
