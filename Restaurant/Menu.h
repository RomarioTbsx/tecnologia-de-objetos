/*
 * Menu.h
 *
 *  Created on: 22 de abr. de 2016
 *      Author: romario
 */

#ifndef MENU_H_
#define MENU_H_

#include <iostream>
#include "Pizza.h"
using namespace std;
class Menu :public Pizza{
	string nomCliente;
	int numMesa;

public:
	Menu();
	~Menu();
	Menu(string,string,int);
	string getCliente();
	int getNumMesa();
};

#endif /* MENU_H_ */
