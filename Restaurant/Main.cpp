/*
 * Main.cpp
 *
 *  Created on: 22 de abr. de 2016
 *      Author: romario
 */

#include "Pizza.h"
#include "Menu.h"
#include "PizzaRestaurant.h"
#include "Empleado.h"
#include "DeliveryMan.h"
#include "Cook.h"
#include "PhoneOperator.h"
int main(){
	Menu menu1("Romario","Italiana",10);
	PizzaRestaurant restaurante("El Pizzero","Cercado","234567");
	restaurante.show();
	PizzaRestaurant menu,empleado;
	DeliveryMan emp1("Juan","CTS",10,"1234567");
	empleado.setDeliveryMan(emp1);
	menu.setMenu(menu1);
	menu.showMenu();
	empleado.showDeliveryMan();


}
