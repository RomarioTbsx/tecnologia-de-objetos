/*
 * DeliveryMan.h
 *
 *  Created on: 22 de abr. de 2016
 *      Author: romario
 */

#ifndef DELIVERYMAN_H_
#define DELIVERYMAN_H_

#include "Empleado.h"
class DeliveryMan:public Empleado {
public:
	DeliveryMan();
	~DeliveryMan();
	DeliveryMan(string,string,double,string);
	void show();
};

#endif /* DELIVERYMAN_H_ */
