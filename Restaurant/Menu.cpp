/*
 * Menu.cpp
 *
 *  Created on: 22 de abr. de 2016
 *      Author: romario
 */

#include "Menu.h"

Menu::Menu() {
	// TODO Auto-generated constructor stub

}

Menu::~Menu() {
	// TODO Auto-generated destructor stub
}

Menu::Menu(string nomCliente,string nomPizza,int numMesa):Pizza(nomPizza){
	this->nomCliente=nomCliente;
	this->numMesa=numMesa;
}

string Menu::getCliente(){
	return nomCliente;
}

int Menu::getNumMesa(){
	return numMesa;
}
